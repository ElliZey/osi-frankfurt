# ReproducibiliTea

## About ReproducibiliTeas all over the world
[ReproducibiliTea journal clubs](https://reproducibilitea.org/) help early career researchers build a local community of ECRs interested in open and reproducible research. It can be very isolating to be one of the few within a research group, or department, that are actively engaged or interested in improving research practices. ReproducibiliTea helps researchers who want to change this. 

We all know how horrible it can be to jump through annoying administrative hurdles or dodge financial barriers to ultimately try to make a positive change. Setting up a ReproducibiliTea Journal Club is easy, free and does not need any admin approval. In a ReproducibiliTea Journal Club, papers are selected that are broadly relevant to the replication crisis and scientific improvements. The journal club is advertised around the department or university, raising awareness of reproducibility and Open Science in the process. The chosen papers are then discussed during regular journal club meetings, often over cups of tea, lunch or snacks. <p> The ReproducibiliTea Journal Club has proven to be a success in Oxford, where it was founded, and has received widespread international recognition. There are now more than 83 other ReproducibiliTea Journal Clubs registered to date, with more joining at an increasingly faster rate.

For more Information see also [ReproducibiliTea journal clubs](https://reproducibilitea.org/). 

## About Frankfurt ReproducibiliTea

Our journal club ReproducibiliTea is organized by the Frankfurt Open Science Initiative, located within the Goethe University, and is open to anyone who is interested in the topic.

This summer term we start with Dr. Jack Taylor from Goethe University Frankfurt who will inform us about "Matchmaking: Matching Stimuli Reproducibly with Code on the 26th of April 2023. We are meeting online from 4pm to 5pm. This term the session on the 3rd of May will be entirely dedicated to an Open Science Café!  We invite you to bring questions, share ideas and thoughts, we highly encourage everyone to discuss. All relevant materials as well as the latest schedule can be found on our [OSF page](https://osf.io/254t7). 

Co-hosts of the Journal Club are [Julia Beitner and Zoë Bolz](https://frankfurt-osi.netlify.app/top/members/). Feel free to contact them, if you have any questions! Also, see our [Twitter](https://twitter.com/OpenScienceFFM) for additonal announcements. 

Send an email with subject Join ReproTea to reprotea-join@dlist.server.uni-frankfurt.de or to beitner@psych.uni-frankfurt.de to subscribe to our newsletter and receive the Zoom link for the meetings. JOIN AT ANYTIME <3

[Journal Club Zoom Meeting](https://uni-frankfurt.zoom.us/j/66016799024?pwd=Yk1URkhUQmIzOVB5ZGJvTG1nZThvUT09)

## The new schedule
  
   
## Past schedules
 
### Winter term 2023/ 2024
![picture](/img/Schedule_ReproTea-winterterm_23-24.png) 

### Summer term 2023
![picture](/img/Schedule_ReproTea-summerterm_23.png) 

### Winter term 2022 / 2023
![picture](/img/Schedule_ReproTea-winterterm_22-23.png)

### Summer term 2022:  
![picture](/img/ReproTea_schedule_summerterm_22.png)

### Winter term 2021 / 2022:  
![picture](/img/ReproTea_schedule_winterterm21-22.png)

### Summer term 2021:  
![picture](/img/Schedule_ReproducibiliTea_Frankfurt_summerterm2021.png)

### Winter term 2020 / 2021:  
![picture](/img/Schedule_ReproducibiliTea_Frankfurt_winterterm2020.png)

![picture](/img/repro-plant.jpg)

