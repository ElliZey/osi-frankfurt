# About

The Frankfurt Open Science Initiative, short Frankfurt OSI, is a group, at the Department of Psychology and Sport Sciences of the Goethe University Frankfurt and other befriended affiliations, who aims to implement, establish, and support open science practices.

### Our Goals 

* providing resources on open science practices, as well as in teaching, 
* offering a 2-year workshop curricula that aids in implementing open science practices <br> in one’s own workflow,
* and strengthening the network between students and staff that are interested in open science.

We are acquiring valuable skills-increasing awareness of the topic of open science by regularly organizing low-threshold events and open formats such as a journal club, 
and by making open science practices of the institute transparent. Additionally we aiming to systematically document everything we do in regards to open science and hope to increase the reach of the community by spreading to more fields.

### How to join us!
We meet on a reguarly basis to talk about open science research practice,  plan events, chat about any open science news or have a cup of Tea at [ReproducibiliTea](https://reproducibilitea.org/journal-clubs/#Frankfurt).


New people are always encouraged to join any of our activities! All workshops / talks and meetings are announced on [EVENTS](https://ellizey.gitlab.io/osi-frankfurt/top/events) and you can always follow us on [Twitter @OpenScienceFFM](https://twitter.com/OpenScienceFFM).  


For more information contact us via e-mail: osi@psych.uni-frankfurt.de 
