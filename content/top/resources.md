# Resources

### Materials by Frankfurt OSI members
* [An Introduction to Python and PsychoPy held by Rebecca Mayer & Dominik Kraft](https://github.com/remayer/WS19_Python_for_Psychologists)
* [Talk: An Introduction to open science by Julia Beitner & Elli Zey](https://osf.io/jx5dc/)

### Materials of our past workshops / events 

#### 2019
* [Schedule and Papers of ReproducibiliTea Frankfurt](https://osf.io/254t7/)
* [Tom Hardwickes Talk "What is this thing called open science" on the 2nd Open Science Day](https://osf.io/6fq2u/)
* [Flavio Azevedo's Talk "FORRT - A Framework for Open and Reproducible Research Training" on the 2nd Open Science Day](https://osf.io/g29eu/)
* Git/GitHub Workshop by Simon Schuge
* [Repository for a docker workshop by Peer Herholz](https://github.com/PeerHerholz/docker_ws_os_frankfurt)

#### 2018
* [Felix Schönbrodt’s Talk “How Open Science Can Solve (Parts Of) The Replication Crisis” on the 1st Open Science Day](https://osf.io/zf6g4/)
* Our Open Science Day was accompanied by a poster session. Some of the posters can be founds here: [Preregistration Poster](https://ellizey.gitlab.io/osi-frankfurt/images/osd_poster_prereg.pdf)  

---

### Data Sharing, Privacy and Consent 
#### Informed Consent
According to the [Declaration of Helsinki](https://www.wma.net/policies-post/wma-declaration-of-helsinki-ethical-principles-for-medical-research-involving-human-subjects/),

>**§32** For medical research using identifiable human material or data, such as
>research on material or data contained in biobanks or similar 
>repositories, physicians must seek informed consent for its collection,
>storage and/or reuse. There may be exceptional situations where consent would 
>be impossible or impracticable to obtain for such research. In such situations
>the research may be done only after consideration and approval of a 
>research ethics committee.

Most recent EU laws also suggest a strong emphasis on informed consent, although [Hessian state 
laws](https://datenschutz.hessen.de/pressemitteilungen/gesetz-zur-anpassung-des-hessischen-datenschutzrechts) grant more freedom to academic research.

Inspired by the [Ultimate Consent Forms](https://open-brain-consent.readthedocs.io/en/master/ultimate.html),
we have developed a general consent form hopefully sufficient to cover most investigations relevant to
conducting psychological and neurocognitive experiments.

You can download the form [here](https://www.dropbox.com/s/chapn9ss0wpz6qt/Probandeninformation_Flexstudie_Teil2.pdf?dl=0).  

---

#### Appropriate Formats

A [guide for sharing data](https://github.com/jtleek/datasharing) by Jeff Leek's group
gives a detailed overview for data sharing best practices.

However, for neuroimaging or other complex data, consider the [BIDS](http://bids.neuroimaging.io) format.  

---

### Other helpful links
#### Open Science Initiatives:
* [List of open science initiatives](https://osf.io/tbkzh/wiki/home/)
* [LMU Open Science Center](https://www.osc.uni-muenchen.de/index.html)
* [SHERPA/RoMEO - Publisher copyright polices & self-archiving](http://sherpa.ac.uk/romeo/index.php)
* [Talk: Maintaining privacy with open data by Ruben Arslan](https://osf.io/9j27d/)

#### Repositories
* [OSF - Open Science Framework](https://osf.io/)
* [Zenodo](https://zenodo.org/)
* [figshare](https://figshare.com/)
* [re3data](https://www.re3data.org)

#### Social Media
* [Open Science on Twitter](https://twitter.com/openscience)
* [Society for the Improvement of Psychological Science (SIPS) on Twitter](https://twitter.com/improvingpsych)

