# Open Science Fellowships

The Frankfurt OSI awarded Open Science Fellowships to 5 recipients in the last two years – both to undergraduate and Phd Students from the Department of Psychology of the Goethe University. The awarded projects range from building knowledge bases to creating open materials and even methods to make science more reproducible.  

We are very proud to announce the fellows:  

---

### Fellows of 2020

- PhD student fellow **Isabelle Ehrlich** for the project "PREMUP-Evidence"
- student fellow **Levi Kumle** for the project "mixed power"


### Fellows of 2021

- student fellow **Klara Gregorova** for the project [Eye-Tracking-BIDS](https://github.com/greckla/Eye-Tracking-BIDS)
- student fellow **Tobias Haase** for the project "Knowledgebase about Actigraphy Research" [@tc_haase](https://twitter.com/tc_haase)
- student fellow **Daniel Probst** for the project "Virtual Work Communication During a Pandemic"

---


Congratulations and we are looking forward to see your progress in the future!  

The Frankfurt Open Science Initiative
