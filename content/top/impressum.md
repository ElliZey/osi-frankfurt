# Impressum
  
_Angaben gemäß § 5 TMG_  

Frankfurt Open Science Initiative  
Theodor-W.-Adorno-Platz 6  
60323 Frankfurt am Main  

**Vertreten durch**

Prof. Dr. Christian Fiebach

**Kontakt**

Telefon: +49 (0)69 798 35335  
E-Mail: osi@psych.uni-frankfurt.de

**Webmaster** 

Zarah Schreiner

---

_Haftungshinweis:_  
Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.  Der Inhalt und die Gestaltung unserer Website sind urheberrechtlich geschützt. Deren Vervielfältigung, Verbreitung und Bearbeitung ist nur mit unserer schriftlichen Zustimmung zulässig.</p>
