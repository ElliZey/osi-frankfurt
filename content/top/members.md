# Members
## Board Members of Frankfurt Open Science Initiative

| Board Members | |
|-|-|
| Tatiana Kvetnaya |
| Lucie Binder |
| Zoë Bolz |
| Christian Fiebach|
| Alexa van Hagen |
| Lea Müller Karoza |
| Gözem Turan |
| Zarah Schreiner |
| Martin Schultze|
| Yee Lee Shing |
| Dominik Welke |
   

## Supporting Members of Frankfurt OSI

- Deliane Bechar  
*Student, Institute of Psychology, Goethe University Frankfurt*
- Garvin Brod  
*DIPF | Leibniz-Institut für Bildungsforschung und Bildungsinformation & Institute of Psychology, Goethe University Frankfurt*
- Stefan Czoschke  
*right° based on science, Frankfurt*
- Erwan David  
*Scene Grammar Lab, Institute of Psychology, Goethe University Frankfurt*
- Fabienne Ennigkeit  
*Sports Pedagogy, Institute for Sport Sciences, Goethe University Frankfurt*
- Aylin Kallmayer  
*Scene Grammar Lab, Institute of Psychology, Goethe University Frankfurt*
- Anna Mavromanoli  
*Center for Thrombosis and Hemostasis, Johannes Gutenberg-Universität Mainz*
- Kai Nehler  
*Psychological Methods with Interdisciplinary Focus, Institute of Psychology, Goethe University Frankfurt*
- Lea Nobbe  
*Education and Human Development, DIPF | Leibniz Institute for Research and Information in Education*
- Sophie Nolden  
*Department of Developmental Psychology, Goethe University Frankfurt*
- Javier Ortiz-Tudela  
*Department of Developmental Psychology, Goethe University Frankfurt*
- Anton Peez  
*Peace Research Institute Frankfurt (PRIF) & Leibniz-Institut Hessische Stiftung Friedens- und Konfliktforschung (HSFK)*
- Elia Samuel Rothenberg  
*Student, Institute of Psychology, Goethe University Frankfurt*
- Jan Luca Schnatz  
*Student, Institute of Psychology, Goethe University Frankfurt*
- Uwe Schulze  
*Didactics of Geography, Human Geography, Goethe University Frankfurt*
- Praveen Sripad  
*Department of Neuroscience, Max-Planck-Institute for Empirical Aesthetics Frankfurt*
- Roland Wagner  
*University Library of Goethe University Frankfurt*
- Sandro Wiesmann  
*Scene Grammar Lab, Institute of Psychology, Goethe University Frankfurt*
- Sabine Windmann  
*General Psychology II, Institute of Psychology, Goethe University Frankfurt*
- Leila Zacharias  
*Student, Institute of Psychology, Goethe University Frankfurt*
- Zefan Zheng  
*Neural Circuits, Consciousness, and Cognition, Max-Planck-Institute for Empirical Aesthetics Frankfurt*
- Jeanette Ziehm-Eicher  
*IDeA-Centre, Department for Education and Development, DIPF | Leibniz Institute for Research and Information in Education*


## ALUMNI Board Members of Frankfurt OSI
- Benjamin Gagl
- Dominik Kraft
- Axel Kohler
- Rebecca Mayer
- Freya Mößig
- Björn Siepe
- Elli Zey
- Julia Beitner
 
 Contact us for our meetings or come by your [events](https://ellizey.gitlab.io/osi-frankfurt/top/events).




	
<TD>  ![picture](/img/christian_fiebach.jpeg)</TD>
      <TD><h3>Christian Fiebach</h3>
	<ul>
	<li><strong> tasks in our OSI: </strong> co-founder, departmental support, financing, political contacts to other initiatives <BR>
	<li><strong>I have experience in:</strong> study planning & pre-registration, grant writing, implementation of open science practices; <BR>
<li> <strong>I want to learn more about</strong>: how to implement transparent research practices in neuroscientific research (in particular data sharing, research data management) <BR>
	<li><strong>You can contact me for:</strong> strategic support, general mentoring <BR>
	<li><strong>research field: </strong> cognitive neuroscience <BR>
	<li><strong>affiliation:</strong> Department of Psychology, Goethe University Frankfurt <BR>
	<ul><li>e-mail: fiebach[@]psych.uni-frankfurt.de <BR>
	<li> visit on <a href="https://fiebachlab.org">homepage</a>
	<li>twitter: @cfiebach
<li>visit on <a href="https://orcid.org/0000-0003-0827-1721" title="ORCID">ORCID</a>
	 </ul></ul></TD>
   </TR>
      <TR>
      <TD>  ![picture](/img/alexa_vonhagen.jpg)</TD>
      <TD><h3>Alexa von Hagen</h3>
	<ul>
	<li><strong> tasks in our OSI: </strong> ReproducibiliTea co-host
	<li><strong>I have experience in:</strong>  preregistration, preprints (OSF)
	<li><strong> you can contact me for: </strong> preregistration, preprints (OSF)
	<li><strong> I want to learn more about: </strong> registered reports
	<li><strong> research field: </strong> educational psychology
	<li><strong>affiliation:</strong> Competence Centre School Psychology Hesse, Institute of Psychology, Goethe University Frankfurt
	<ul><li>e-mail: vonHagen[@]em.uni-frankfurt.de 
	<li>twitter: @alexavonhagen </ul></ul></TD>
   </TR>
   <TR><TD>  ![picture](/img/lea_karoza.jpg)</TD>
      <TD><h3> Lea Müller Karoza</h3>
	<ul><li> <strong> tasks in our OSI: </strong> maintaining of Frankfurt osi homepage
	<li><strong>I have experience in:</strong> preregistration
	<li><strong>You can contact me for:</strong> anything regarding the homepage or ReproducibiliTea
	<li><strong> research field: </strong> scene perception, affordances
	<li><strong>affiliation:</strong> Scene Grammar Lab, Goethe University Frankfurt
	<ul><li>e-mail: lea.karoza[@]stud.uni-frankfurt.de
	<li> twitter: <a href="https://www.twitter.com/Lea_Lexi_" title="twitter">@Lea_Lexi_</ul></ul></TD>
   </TR>
<TD>  ![picture](/img/thomas_loesch.jpg)</TD>
      <TD><h3>Thomas Lösch</h3>
	<ul>
	<li><strong> tasks in our OSI: </strong> Being present, supporting the connection with the DIPF 
	<li><strong>I have experience in:</strong>  Research data management, preregistration, R
	<li><strong>You can contact me for:</strong> (Giving workshops in) research data management, R (data visualization), Open Science in education sciences
	<li><strong>research field: </strong> Psychology, education sciences, Meta-science in education
	<li><strong>affiliation:</strong> DIPF | Leibniz Institute for Research and Information in Education
	<ul>
	<li>e-mail: loesch[@]dipf.de
	<li>visit on <a href="https://www.dipf.de/de/institut/personen/loesch-thomas" title="homepage">homepage</a> 
	<li>visit on <a href="https://orcid.org/0000-0002-2582-6787" title="ORCID">ORCID</a>
	<li>visit on <a href="https://www.researchgate.net/profile/Thomas_Loesch" title="Researchgate">Researchgate</a>
	<li>visit on <a href="https://osf.io/3d924/" title="OSF">OSF</a> </ul></ul></TD>
   </TR>
   <TR>
<TD>  ![picture](/img/gozem_turan.JPG)</TD>
      <TD><h3>Gözem Turan</h3>
	<ul>
	<li><strong> tasks in our OSI: </strong> ReproducibiliTea co-host, sharing ideas
	<li><strong> research field: </strong> Memory and Electroencephalography
	<li><strong>affiliation:</strong> Lifespan Cognitive and Brain Development Lab., Goethe University Frankfurt
	<ul> <li>e-mail: turan[@]psych.uni-frankfurt.de
	<li>twitter: @gozemturan
	<li>visit on <a href="https://www.researchgate.net/profile/Goezem_Turan">ResearchGate</a></ul> </ul></TD>
   </TR>
   <TR>
<TD>  ![picture](/img/martin_schultze.jpg)</TD>
      <TD><h3>Martin Schultze</h3>
	<ul>
	<li><strong> tasks in our OSI: </strong> spreading OS ideas in stats and methods courses, <br>
	organization of the OS summer school course, patron of an award for replication studies by undergrads
	<li><strong>I have experience in:</strong>  Preregistration (OSF, asPredicted), <br> 
	writing reproducible and replicable analysis code and documentation via R, writing open code & R-packages, simulation-based power-analyses
	<li><strong>You can contact me for:</strong> Questions on using R and its automatic integration in reports as well as writing R packages
	<li><strong>I want to learn more about</strong>: Replicability of methods of statistical (and machine) learning
	<li><strong> research field: </strong> quantitative psychology, statistical methods, assessment, multi-trait multi-method analysis
	<li><strong>affiliation:</strong> Psychological methods with interdisciplinary focus, <br>Institute of Psychology, Goethe University Frankfurt
	<ul><li>e-mail: schultze[@]psych.uni-frankfurt.de
	<li> visit on <a href="https://www.psychologie.uni-frankfurt.de/75520710/Abteilung_f%C3%BCr_Psychologische_Methoden_mit_interdisziplin%C3%A4rer_Ausrichtung" title="homepage">homepage</a></ul></ul></TD>
   </TR>
   <TR>
<TD>  ![picture](/img/yee_lee_shing.jpg)</TD>
      <TD><h3>Yee Lee Shing</h3>
	<ul>
	<li><strong> tasks in our OSI: </strong>  teaching ideas of open science to students through seminars/lectures/thesis supervision
	<li><strong>I have experience in:</strong>   pre-registration (OSF, AsPredicted)
	<li><strong>You can contact me for:</strong> pre-registration
	<li><strong>research field: </strong> developmental psychology
	<li><strong>affiliation:</strong> LISCO lab, Institute of Psychology, <br> Goethe University Frankfurt
	<ul><li> visit on <a href="http://www.psychologie.uni-frankfurt.de/50042944/50_entwicklung" title="homepage">homepage</a> 
	<li>twitter:  @yeeleeshing1</ul></ul></TD>
   </TR>
      <TR>
<TD>  ![picture](/img/bjoern_siepe.jpg)</TD>
      <TD><h3>Björn Siepe</h3>
	<ul>
	<li><strong> tasks in our OSI: </strong> ReproducibiliTea co-host
	<li><strong>I have experience in:</strong> preregistration, being an editor in a fully open-access journal for students, creating reproducible R scripts
	<li><strong>You can contact me for:</strong> ReproducibiliTea, preregistration, publishing opportunities for undergraduate students
	<li><strong>research field: </strong> clinical psychology
	<li><strong>affiliation:</strong> Master Student in Psychology, Goethe University Frankfurt
	<ul><li>e-mail: b.siepe[@]stud.uni-frankfurt.de
	<li>twitter:  @b_siepe</ul></ul></TD>
   </TR>
   <TR><TD>  ![picture](/img/dominik_welke.png)</TD>
      <TD><h3> Dominik Welke </h3>
	<ul><li><strong> tasks in our OSI: </strong>  OSI fellowship committee (for undergraduates), link to MPI EA
	<li><strong>I have experience in:</strong> OpenAccess policy, BIDS (M/EEG), MNE/python, GIT, preregistration, power analysis
	<li><strong> research field: </strong> neuroscience of aesthetics, EEG/physiology
	<li><strong>affiliation:</strong> Max-Planck-Institute for Empirical Aesthetics Frankfurt
	<ul><li> e-mail:  dominik.welke[@]ae.mpg.de 
<li> visit on <a href="https://www.aesthetics.mpg.de/institut/mitarbeiterinnen/dominik-welke.html" title="homepage">homepage</a>
<li> visit on <a href="https://orcid.org/0000-0002-5529-1998" title="Orcid">Orcid</a> 
<li> visit on <a href="https://osf.io/xfwqp/" title="OSF">OSF</a> 
<li> visit on <a href="https://profiles.impactstory.org/u/0000-0002-5529-1998" title="Impactstory">Impactstory</a> </ul></ul></TD>
   </TR>
   <TR>
   <TR><TD>  ![picture](/img/elli_zey.jpg)</TD>
      <TD><h3> Elli Zey</h3>
	<ul><li> <strong> tasks in our OSI: </strong> ReproducibiliTea co-host, developing and maintaining of Frankfurt osi homepage
	<li><strong>I have experience in:</strong>preregistration, open source software alternatives, programming/sripting in R/php/markdown/html
	<li><strong>You can contact me for:</strong> anything regarding the homepage or ReproducibiliTea
	<li><strong> research field: </strong> moral decision making, 
	<li><strong>affiliation:</strong> General Psychology II, Goethe University Frankfurt
	<ul><li>e-mail: zey[@]psych.uni-frankfurt.de
<li> visit on <a href="https://www.psychologie.uni-frankfurt.de/76552949/Elli_Zey" title="homepage">homepage</a>
	<li>twitter: @elli_zey</ul></ul></TD>



