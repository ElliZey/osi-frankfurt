# Events

## Coming up: Winters semester 2022/2023

- [6th round of ReproducibiliTea](https://frankfurt-osi.netlify.app/post/reproducibilitea-winter-term-2022-23/), every 2nd Wednesday 4pm
- [Perspektive offene Wissenschaft](https://aktuelles.uni-frankfurt.de/event/rmu-veranstaltung-zu-open-science-2022-perspektive-offene-wissenschaft-rmu-2022/) @ RMU 2022, November 30th
	- Workshop [Kick-Start to Open Source Software in Science and Everyday Life](https://osf.io/yr6u2) by Elli Zey

## Past events  

## Summer semester 2022
- [OPEN SCIENCE FORUM](https://www.ub.uni-frankfurt.de/veranstaltungen/open-science2022.html), June 30th and July 1st
	- Hackathon [Frankfurt Open Science Initiative - who we are, what we do, and why we need YOU.](https://doi.org/10.21248/gups.68792) by Julia Beitner
	- Workshop [Kick-Start to Open Source Software in Science and Everyday Life](https://osf.io/npyc3) by Elli Zey
- [5th round of ReproducibiliTea](https://frankfurt-osi.netlify.app/post/reproducibilitea-summer-term-2022/), every 2nd Wednesday 4pm
- [Open Science Coffee](https://frankfurt-osi.netlify.app/post/open-science-coffee-summer-term-2022) for any questions related to Open Scholarship, join the montly Open Science Coffee

### Winter semester 2021 / 2022  
- [4th round of ReproducibiliTea](https://frankfurt-osi.netlify.app/post/reproducibilitea-winterterm-2020-2021/), every 2nd Wednesday 4pm

### Summer semester 2021
- [3rd round of ReproducibiliTea](https://frankfurt-osi.netlify.app/post/reproducibilitea-summer-term-2021), every 2nd Wednesday 4pm
- [Open Science Coffee](href="https://frankfurt-osi.netlify.app/post/open-science-coffee-summer-term-2021/): monthly coffee on Wednesdays 5pm after each 1st ReproducibiliTea of the month

- [Open Science Fellowship Ceremony](https://frankfurt-osi.netlify.app/post/std-open-science-fellowhship-ceremony) on June 30th 2021
- [Workshop "Preregistration (in Psychology)" by Lisa Spitzer, ZPID](https://frankfurt-osi.netlify.app/post/announcing-workshop-preregistration-in-psychology/)

- [Workshop "Design and Analysis of Replication Experiments] by Leonhard Held, Charlotte Micheloud and Samuel Pawel, University of Zurich on March 12th

### Summer semester 2020

- [Contribution](https://frankfurt-osi.netlify.app/post/our-student-fellow-leah-presented-her-work/) to Vision Science Conference by our fellow Levi Kumle
- Cancellation/postponement of Open Science Fellowships Event on the 22nd of April
- 2nd round of ReproducibiliTea, every 2nd Wednesday 4pm

### Winter semester 2019 / 2020

- [RepliCATS Workshop](https://replicats.research.unimelb.edu.au/) on , March 10th coffee session 2-4pm. RepliCATS Workshops can be done from everywhere and online, alone or in groups- so if you missed the session, join anytime!  
- 1st round of ReproducibiliTea, every 2nd Wednesday 4pm

#### Second OPEN SCIENCE DAY, January 15
- Tom Hardwickes Talk [What is this thing called open science](https://osf.io/6fq2u/)
- Flavio Azevedo's Talk [FORRT - A Framework for Open and Reproducible Research Training](https://osf.io/g29eu/)  

### Summer semester 2019
- [Docker workshop](https://github.com/PeerHerholz/docker_ws_os_frankfurt) by Peer Herholz, April 2019
- [Git/GitHub Workshop](https://smonsays.github.io/git_workshop/#/iii.-version-control) by Simon Schuge

### Winter semester 2018 / 2019
- WORKSHOP: Forschungsdatenmanagement, November 30 </h4>
The goal of this workshop is to introduce and practice procedures for handling research data in an open and reproducible way. 
	- Erich Weichselgartner (ZPID) introduced guiding principles to make data Findable, Accessible, Interoperable, and Reusable, the [FAIR principles and DataWiz](https://gitlab.com/ElliZey/frankfurt-osi-materials/-/tree/master/2018-11_research_data_management%2F2018-11_research_data_management), an automated assistant for the management of psychological research data developed by ZPID
	- Annett Wilde (DIPF) introduced [REDCap](https://gitlab.com/ElliZey/frankfurt-osi-materials/-/tree/master/2018-11_research_data_management%2F2018-11_research_data_management), a web application for building and managing online surveys and databases.
	- Benjamin Gagl introduced BIDS (Brain Data Imaging Structure), a data storage standard for behavioral or brain data, which is the basis for using a growing number of open data processing pipelines (BIDS apps) allowing reproducible data analysis.
- WORKSHOP: [Power Analyses in Psychological Science](https://gitlab.com/ElliZey/frankfurt-osi-materials/-/tree/master/2018-10_power_analysis_DLakens), October 22 - 23. Daniel Lakens introduced the theoretical concept of Power Analyses and provided an overview of advantages and disadvantages of different methods. The focus of the workshop was on Hands On Experiences in order to learn how to compute basic indicators of power and how they can be used for planning, and conducting new studies. Further, related topics of meta-analythical approaches were discussed.
- WORKSHOP: Preregistration, October 9: Pre-registration is one of the means that the scientific community is increasingly using to counter the replication crisis: In this one-day workshop, we 1) conceptually introduced the attendees to pre-registration, 2) prepared the attendees for practical possibilities and difficulties, and 3) provided support in preparing own pre-registrations.

### Summer semester 2018
#### First OPEN SCIENCE DAY, June 27th
- Felix Schönbrodt’s Talk [How Open Science Can Solve (Parts Of) The Replication Crisis”](https://osf.io/zf6g4/)
- Our Open Science Day was accompanied by a poster session.

