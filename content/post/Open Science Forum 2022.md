---
layout:     post
title:      "Join the OPEN SCIENCE FORUM on June 30th and July 1st"
subtitle:   "Great keynotes, workshops, panel, and social gatherings on Open Science"
date:       2022-06-05
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> We are happy to announce the first university wide OPEN SCIENCE FORUM</strong><br>

<p>We invite everyone to join the <a href="https://www.ub.uni-frankfurt.de/veranstaltungen/open-science2022.html" target="_blank" rel="nofollow">Open Science Forum</a></p>

<p>On <strong>Day 1, June 30th,</strong> we invite you to join us at the Lobby of the PA Building (Präsidiums Gebäude) 2pm to 6:30 pm and then afterwards for a social gathering at the Sommergarden next to the Casino Building on Campus Westend: </p>
<br>
14:00-14:10 	<strong>Opening Ceremony</strong>, President Prof. Dr. Enrico Schleiff<br>

14:15-15:00 	<strong>Keynote 1, Transition to Open Science: The Why and the How.</strong> Prof. Dr. Frank Miedema (University Utrecht)<br>

15:00-15:30 	<strong>The Replication Crisis in Science: An Overview and Current Results</strong>, Dr. Axel Kohler<br>

15:30-16:00 	<strong>Open Science in Public Private Partnerships</strong>, Dr. Susanne Müller-Knapp / Dr. Claudia Tredup<br>

16:00-16:15 	Coffee Break<br>

16:15-16:45 	<strong>Open Educational Resources (OER) und digitale Hochschulbildung</strong>, Dr. Uwe Schulze<br>

16:45-17:15 	<strong>Frankfurt Open Science Initiative - who we are, what we do, and why we need YOU</strong>, Julia Beitner<br>

17:15-18:30 	<strong>Panel discussion on Open Science</strong>, Chair: Vice President Prof. Dr. Michael Huth<br>

Panelists: Prof. Dr. Barbara Alge, Prof. Dr. Hannah Elfner, Prof. Dr. Christian<br> Fiebach, Lea Müller Karoza B.Sc., Prof. Dr. Marc Rittberger<br>


<p>On Day 2, July 1st, we have parallel Workshops in the Seminar Pavillion planned in the morning and will then continue with Keynote 2 and a "Wrap up & outlook" in the Casino Nebengebäude: </p>

09.15-10:45 	<strong>Kick-Start to Open Source Software in Science and Everyday Life</strong>, Elli Zey, SP 0.03

09.15-10:45 	<strong>Come in and find out: Kick-off to join the Frankfurt Open Science Initiative</strong>, Julia Beitner, SP 0.04

09.15-10:45 	<strong>Offen, frei und kollaborativ?! Open Educational Resources (OER) in der Hochschullehre einsetzen</strong>, Dr. Uwe Schulze / Luca Mollenhauer, SP 1.01

11:15-12:00 	<strong>Keynote 2: Open Science, Version 3.0: Breaking Down Barriers for Equitable and Efficient Research Communication</strong>, Dr. Wendy Patterson (Scientific Director Beilstein-Institute), Nebengebäude Casino NG 1.741

12:00-12:30 	<strong>Wrap up & outlook</strong>, CIO Ulrich Schielein, Nebengebäude Casino NG 1.741

<strong> How to participate? </strong> 

<p>Sign up <a href="https://anmeldung.studiumdigitale.uni-frankfurt.de/allgemein/index.php?veranstaltung=osf22" target="_blank" rel="nofollow">here</a> and join for the keynotes, workshops, panel, and social gatherings! </p>

We are looking forward to seeing you and chatting about all things Open Science! <br>

Best wishes on behalf of the ReproTea Team and the Frankfurt OSI,<br>
Elli

