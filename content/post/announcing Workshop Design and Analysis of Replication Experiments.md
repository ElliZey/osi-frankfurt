---
layout:     post
title:      "Workshop “Design and Analysis of Replication Experiments”"
subtitle: "– with an introduction to the R package ReplicationSuccess"
date:       2021-02-26
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---

Invitation to join the Workshop on Replications- introducing the R package ReplicationSucess


Leonhard Held, Charlotte Micheloud and Samuel Pawel, University of Zurich

*Date: March 12, 2021,
Time: 9:00 - 13:00 h,
Location: Online (Zoom link will be provided)*
 
Replication studies are increasingly conducted in order to confirm original findings. However, there is currently no consensus on how to design such studies and to define replication success. The purpose of this tutorial is to describe and compare statistical approaches for the design and analysis of replication studies. The standard method based on significance is discussed, as well as alternative approaches based on effect sizes. Participants will learn how to use the R-package ReplicationSuccess and will apply the methods to real data from large- scale replication projects. Prerequisites include basic knowledge of R and familiarity with standard concepts of statistical inference.

<p><strong>Trainers:</strong></p>
<p>Leonhard Held is Professor of Biostatistics at the University of Zurich and Director of the
Center for Reproducible Science. Charlotte Micheloud and Samuel Pawel are PhD students
developing new methodology for the design and analysis of replication experiments.</p>

Please sign up here: https://forms.gle/CRfASFG85kryPjER7

Please note that the number of participants is limited to 30. In case the number of participants is already reached, we will inform you.



