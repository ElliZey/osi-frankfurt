---
layout:     post
title:      "Cancelling: Workshop: Liberating academic work(s)"
subtitle: "– a workshop on Creative Commons licences"
date:       2023-01-10
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---

Dear everyone,

we hope you had a great start in 2023. Unfortunately we have to announce the cancellation of the Workshop "Liberating academic work(s) – a workshop on Creative Commons licenses". Hopefully, we can reschedule for the upcoming terms. We will let you know as soon as possible.

Cancelled: 
Date: January, 17 2023
Time: 3pm-4pm 
Location: Online

On behalf of the Frankfurt Open Science Initiative and all the best,

Elli
