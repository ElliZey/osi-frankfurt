---
layout:     post
title:      "Join 4th Session ReproducibiliTea"
subtitle:   
date:       2021-01-12
author:     "Julia Beitner for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> Happy New Year! </strong><br>

We hope you had some relaxing holidays and are all safe and well. 
We will reconvene tomorrow, discussing multiverse analyses in our fourth session. The meeting will take place from 4-5 pm (sharp, German time, i.e., UTC+1). <br>

Send an email with subject Join ReproTea to reprotea-join@dlist.server.uni-frankfurt.de or to beitner@psych.uni-frankfurt.de to subscribe to our newsletter and receive the Zoom link for the meetings.

We are looking forward to seeing you and chatting about all things Open Science! 

Best wishes on behalf of the ReproTeaTeam,
Julia

 <p>

![picture](/img/Schedule_ReproducibiliTea_Frankfurt_winterterm2020.png)




