---
layout:     post
title:      "Start of ReproducibiliTea in Summer Term 2022 "
subtitle:   "Schedule and Information"
date:       2022-04-19
author:     "Repro-Tea Team for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> Announcing the start ReproducibliTea in this summer term </strong><br>

Dear All, 

we are very proud to announce: our ReproTea goes in the 5th round: every second Wednesday we meet again from 4-5 pm (sharp, German time, i.e., UTC+1)! This year's sessions are especially friendly if you are new to Open Science- so if you feel like getting to know open science better, this is your chance! We are looking forward to seeing new and familiar faces

The papers and materials, as well as all materials from the last terms, can be found here: https://osf.io/254t7/

<strong> How to join? </strong><br>
Send an email with subject Join ReproTea to reprotea-join@dlist.server.uni-frankfurt.de to  subscribe to our newsletter or go directly to the Zoom Link for the sessions: https://tinyurl.com/ReproTeaFFM 

Best wishes and we are looking forward to seeing you!
Alexa, Björn, Elli, Gözem, and Julia


The schedule:

![picture](/img/ReproTea_schedule_summerterm_22.png)

 <p>





