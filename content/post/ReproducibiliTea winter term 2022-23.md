---
layout:     post
title:      "Start of ReproducibiliTea in winter term 2022-23"
subtitle:   "Schedule and Information"
date:       2022-10-12
author:     "Repro-Tea Team for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> Announcing the start of ReproducibliTea in this winter term </strong><br>

Dear All, 

we are very excited to announce our new 💥 ReproTea schedule 💥 with interesting topics from Open Educational resources to hands on topics like how to deal with pyramids of data analysis. 

This term our Frankfurt ReproTea goes in the 6th round: every second Wednesday we meet again from 4-5 pm (sharp, German time, i.e., UTC+1). We have great guests: Julien Irmer, Roland Wagner, Angelika Stefan, Rebecca Willen, Lisa Spitzer; Plus two sessions by our hosts Gözem Turan & Elli Zey.

The papers and all materials from the last terms, can be found on <a href="https://osf.io/254t7/" target="_blank" rel="nofollow">OSF</a> and here you go directly to the <a href= "https://osf.io/zng2d" target="blank" rel="nofollow"> schedule </a>

<strong> How to join? </strong><br>
Send an email with subject Join ReproTea to reprotea-join@dlist.server.uni-frankfurt.de to  subscribe to our newsletter or go directly to the Zoom Link for the sessions: https://tinyurl.com/ReproTeaFFM 

Best wishes and we are looking forward to seeing you!
Gözem, Julia, and Elli

Link to the meetings: 
<a href="https://uni-frankfurt.zoom.us/j/92339090315?pwd=UTVHLytPa1ZVMjRsU0xsN2w1QzFkQT09" target="_blank" rel="nofollow">Journal Club Zoom Meeting</a>

The schedule:
![picture](/img/Schedule_ReproTea-winterterm_22-23.png)
 <p>
