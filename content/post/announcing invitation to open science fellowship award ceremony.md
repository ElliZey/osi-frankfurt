---
layout:     post
title:      "Invitation to Open Science Fellowship Award Ceremony - 3.7., 15:00 s.t."
subtitle: ""
date:       2024-06-24
author:     "Zarah Schreiner for the Frankfurt Open Science Initiative"
image:      ""
---

Dear all,

the Frankfurt Open Science Initiative cordially invites you to the inaugural Frankfurt Open Science Fellowship Ceremony that will take place on July, 3rd from 3-5 pm in PEG 1G.135.

The Open Science Fellowships are annually awarded to dedicated undergraduate and PhD students from the Department of Psychology of the Goethe University, who have made a particular contribution to Open Science in the course of their projects. We are happy that this year's Fellows will present their work to a broader audience. Besides lightning-talks, there will be an opportunity to get in touch with our Fellows and get to know the next generation of Open Scientists here from Goethe University.

We explicitly invite Bachelor's and Master's students to attend the event. It poses a wonderful opportunity to network, learn about the progress in Open Science and find out about the different ways the initiative can support you and your projects and help making your plans come true.

We are looking forward to celebrating the Fellowships with you together and chat over refreshments after the official part of the event.

![picture](/img/OSI_Fellowships_2024_Werbung-2.png)

<p>
