---
layout:     post
title:      "Watch online: 2nd OPEN SCIENCE DAY at Goethe University"
subtitle:   "Videos of the talks by Tom Hardwick and Flavio Azevedo"
date:       2020-03-23
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---
Dear all,

since we all have more or less time in the evenings, we want to let you know, that your Open Science Day talks of @Tom_Hardwicke and @Flavio_Azevedo_ can be watched anytime here: <a title="Link to Videos" href="https://video01.uni-frankfurt.de/Mediasite/Play/0861a43dda8a40eab129e7ac358c50191d">Video to talks</a> <p>


![picture](/img/OpenScienceDay2020.png)




