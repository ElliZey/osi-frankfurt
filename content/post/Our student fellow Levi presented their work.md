---
layout:     post
title:      "Our student fellow Levi presented their work"
subtitle:   ""
date:       2020-07-20
author:     "Julia Beitner for the Frankfurt Open Science Initiative"
image:      ""
---
For the first time, we awarded the Open Science Frankfurt Fellowships for one student (500€) and one PhD-student (1000€) in early 2020. These fellowships support planned/current projects that include Open Science in any fashion, and were awarded to Levi Kumle (student fellow) and Isabelle Ehrlich (PhD-student fellow) for their inspiring and open-science-supporting projects.
Now our student fellow Levi presented her work at the virtual Vision Science Conference 2020! Leah created an R package to calculate power analyses for general linear mixed models. Kudos, Levi!
Find her poster here: https://lkumle.github.io/vitae/assets/power_mixedmodels_VSS2020.pdf
The preprint here: https://psyarxiv.com/vxfbh
And most importantly, the package here: https://github.com/DejanDraschkow/mixedpower 







