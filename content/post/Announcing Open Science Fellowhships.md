---
layout:     post
title:      "Announcing our Open Science Fellowships"
subtitle: ""
date:       2021-07-28
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---

Dear all,

the Open Science Fellowships were awarded to 5 recipients in
the last two years – both to undergraduate and Phd Students from the
Department of Psychology of the Goethe University. The
awarded projects range from building knowledge bases to creating open
materials and even methods to make science more reproducible.<br>
We are very proud to announce the fellows:

<h2>Awarded Open Science Fellows of 2020</h2>

<li> PhD student fellow <strong>Isabelle Ehrlich</strong> for the project "PREMUP-Evidence"</li>

<h2>Awarded Open Science Fellows of 2021</h2>

<li> student fellow  <strong>Klara Gregorova</strong> for the project <a href= "https://github.com/greckla/Eye-Tracking-BIDS" rel="nofollow">"Eye-Tracking-BIDS"</a> </li>
<li> student fellow  <strong>Tobias Haase</strong> for the project "Knowledgebase about Actigraphy Research"</li>
<li> student fellow <strong> Daniel Probst</strong> for the project "Virtual Work Communication During a Pandemic"</li>

Congratulations and we are looking forward to see your progress in the future!<br>

The Frankfurt Open Science Initiative
