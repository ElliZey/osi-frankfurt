---
layout:     post
title:      "Start of Winter Term ReproducibiliTea"
subtitle:   
date:       2020-11-11
author:     "Julia Beitner for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> Announcing the start of this Winter terms ReproducibliTea </strong><br>

This terms schedule is a broad mix of open science enthusiast to rather critical voices, we hope to have great discussions!

The schedule:

![picture](/img/Schedule_ReproducibiliTea_Frankfurt_winterterm2020.png)

<strong> How to join? </strong> 

Send an email with subject Join ReproTea to reprotea-join@dlist.server.uni-frankfurt.de or to beitner@psych.uni-frankfurt.de to subscribe to our newsletter and receive the Zoom link for the meetings.

We are looking forward to seeing you and chatting about all things Open Science! 

Best wishes on behalf of the ReproTeaTeam,
Julia

 <p>





