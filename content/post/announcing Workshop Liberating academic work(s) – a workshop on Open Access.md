---
layout:     post
title:      "Workshop: Licencing Open Access publications, Creative Commons and legal considerations"
subtitle: ""
date:       2024-02-18
author:     "Zarah Schreiner for the Frankfurt Open Science Initiative"
image:      ""
---

We are excited to announce the Workshop "Licencing Open Access publications, Creative Commons and legal considerations"
 
The workshop is held by Jakob Jung

Date: March, 06 2024
Time: 3pm-4:30pm
Location: PEG 5.G129 

Open Access Publishing is becoming increasingly important in academia. As part of Open Science, OA enables free access to research results for anyone, anywhere. However, researchers face a sheer labyrinth of challenges: Publication options, financing, weighing reputation against idealism, OA mandates set by funding institutions, legal questions etc.
The workshop wants to address these challenges, provide background and a critical outlook on the current state of the OA transformation, and offer best practices for day-to-day research publishing. 

Participants are invited to suggest one or more topics from the following list that will be discussed in detail. Additionally, they can suggest their own ideas or bring practical questions arising from their research.
* The different paths to Open Access (Gold, Green, Hybrid, Diamond)
* Financing Gold Open Access at GU
* Licencing Open Access publications, Creative Commons and legal considerations
* Self-Archiving (Green OA): Best practices and services
* The Open Access transformation: background, current state (e.g. DEAL) and critical outlook
* Scholar-led Open Access

Use this poll to vote for your preferred topics until 29th of February: https://terminplaner4.dfn.de/CdHRlN6WncCBsohr

<p> Jakob Jung works in the Open-Access-Team of the Goethe University Library <br>
You can find more information about the Open Access Services of the Goethe University Library here: <a href="https://www.ub.uni-frankfurt.de/publizieren/home_en.html" target="_blank" rel="nofollow">Open Access Publishing at Goethe University</a>
</p>

Use this poll to register for the workshop (also you can ask to be sent the slides or be contacted for Open Access consultation): https://terminplaner4.dfn.de/rtmriGqZWn7yOKlX 

[Here](/post/workshop_slides.pdf) you can find the slides.
