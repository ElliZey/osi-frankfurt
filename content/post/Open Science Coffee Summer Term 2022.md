---
layout:     post
title:      "Join the Open Science Coffee"
subtitle:   "to discuss anything Open Scholarship related"
date:       2022-03-05
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> We are happy to announce, that we continue the Open Science Coffee </strong><br>

We invite everyone to the monthly Open Science Coffees from 5pm to 6pm after every 1st ReproTea of the month. Whether you want to discuss which preregistration template to use or how to get started with Data Sharing- every question is welcome to be discussed. Everybody is invited to bring questions and we highly encourage everyone to help finding answers. Meetings will be under the same link as the ReproTea Meetings, so it’s optional to simply stay for the Coffee.

<strong> What are the dates and time? </strong> 
This term the dates are: April 20th, May 18th, June 15th and July 13th. And the Open Science Coffee starts at 5pm (Berlin Time) and goes until 6pm. 

<strong> How to join? </strong> 

Send an email with subject Join Open Science Coffee / ReproTea to reprotea-join@dlist.server.uni-frankfurt.de or go directly to the Zoom Link for the sessions: https://tinyurl.com/ReproTeaFFM 

We are looking forward to seeing you and chatting about all things Open Science! 

Best wishes on behalf of the ReproTea Team and the Frankfurt OSI,
Elli
 <p>
