---
layout:     post
title:      "Save the Date - 30.06 - Open Science Fellowship Ceremony"
subtitle: "June, 30th from 4-6 pm"
date:       2021-05-19
author:     "Dominik Kraft for the Frankfurt Open Science Initiative"
image:      ""
---

Dear all,

the Frankfurt Open Science Initiative cordially invites you to the
inaugural Frankfurt Open Science Fellowship Ceremony that will
virtually take place on June, 30th from 4-6 pm.


The Open Science Fellowships were already awarded to 5 recipients in
the last two years – both to undergraduate and Phd Students from the
Department of Psychology of the Goethe University – and we are happy
that our Fellows will present their work to a broader audience. The
awarded projects range from building knowledge bases to creating open
materials and even methods to make science more reproducible.
Besides brief lightning-talks, there will be an opportunity to get in
touch with our Fellows and get to know the next generation of Open
Scientists here from Goethe University.

Please register under https://forms.gle/f58A3KftJ8re7qcm6 for the
event. The Zoom link will be send prior to the meeting.

We are looking forward to celebrating the Fellowships with you together!

Julia Beitner & Dominik Kraft 



