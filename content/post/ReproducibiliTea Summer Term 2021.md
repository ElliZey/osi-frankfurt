---
layout:     post
title:      "Start of Summer Term 2021 ReproducibiliTea"
subtitle:   "Schedule and Informations"
date:       2021-04-12
author:     "Repro-Tea Team for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> Announcing the start of this Summer terms ReproducibliTea </strong><br>

Dear All, 

our ReproTea goes in the 3rd round: every second Wednesday we meet again from 4-5 pm (sharp, German time, i.e., UTC+1) and we proudly present the new schedule of our ReproducibiliTea Journal Club starting on the 14th of April! 

<strong> How to join? </strong><br>
Send an email with subject Join ReproTea to reprotea-join@dlist.server.uni-frankfurt.de or to beitner@psych.uni-frankfurt.de to subscribe to our newsletter and receive the Zoom link for the meetings. Find the papers at: https://osf.io/254t7/

Best wishes and we are looking forward to seeing you!
Gözem, Elli, Alexa, and Julia

This terms schedule is a broad mix of structural challanges in Open Science to very specific open science topics!

The schedule:

![picture](/img/Schedule_ReproducibiliTea_Frankfurt_summerterm2021.png)

 <p>





