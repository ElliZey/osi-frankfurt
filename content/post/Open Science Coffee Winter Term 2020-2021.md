---
layout:     post
title:      "Start of Open Science Coffee"
subtitle:   
date:       2021-02-05
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> Announcing the start of this Winter terms Open Science Coffee </strong><br>

We are proud to announce the official start of the Open Science Coffee: Starting on February 10th 2021 we would like to invite you to monthly Open Science Coffees from 5pm to 6pm after every 1st ReproTea of the month. Whether you want to discuss which preregistration template to use or how to get started with Data Sharing- every question is welcome to be discussed. Everybody is invited to bring questions and we highly encourage everyone to help finding answers. Meetings will be under the same link as the ReproTea Meetings, so it’s optional to simply stay for the Coffee.

<strong> How to join? </strong> 

Send an email with subject Join Open Science Coffee / ReproTea to reprotea-join@dlist.server.uni-frankfurt.de or to beitner@psych.uni-frankfurt.de to subscribe to our newsletter and receive the Zoom link for the meetings.

We are looking forward to seeing you and chatting about all things Open Science! 

Best wishes on behalf of the ReproTeaTeam,
Elli

 <p>





