---
layout:     post
title:      "Workshop: Open Research Data – Bright new future or just a flash in the pan?"
subtitle:   "with Yves Vincent Grossmann (University Library Frankfurt)"
date:       2024-06-24
author:     "Tatiana Kvetnaya"
image:      ""
---

Dear all, 

we are pleased to be able to offer a catch-up date for the postponed workshop which will take place on 1st of July. 

The third workshop in our series *Liberating academic Work(s)* is dedicated to open research data. As always, we invite everyone - whether GU PhD students, research data specialists, or anyone interested in open science - to this exciting workshop by Yves Grossmann:

## Open Research Data – Bright new future or just a flash in the pan?

- **Speaker:** Yves Vincent Grossmann (University Library Frankfurt)

- **Date and Time:** 01. July 2024, 15:00–17:00

- **Location:** SP 2.04 (Seminarpavillon Westend) and Zoom

- **Registration:** https://terminplaner4.dfn.de/openresearchdata2024 

Open research data is frequently mentioned, supported by research agencies and widely reused. What exactly does “open research data” (ORD) mean? Does it make sense? And if so, how can I do that and, above all, where?

The workshop will first discuss what you have to imagine by ORD and what significance they already have in everyday life inside and outside of academia. Following on from this, the workshop will show how to develop ORD and deal with it. This is specifically integrated with data services that are already available within Goethe University. Ultimately, the workshop will close with a critical reflection and arguments against ORD. Depending on the context, there are also good arguments against ORDs.

### Keywords:
- Open Research Data (ORD) in the context of Open Science
- Examples and practices of ORDs
- Good scientific practice and normative aspects for ORDs
- Publishing research data open
- Reproducibility and open research data
- Reflection on why ORDs can also be problematic
