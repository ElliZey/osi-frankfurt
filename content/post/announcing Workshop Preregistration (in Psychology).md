---
layout:     post
title:      "Workshop “Preregistration (in Psychology)”"
subtitle: "– with an introduction to the R package prereg"
date:       2021-03-01
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---

Invitation to join the Workshop on Preregistration- introducing the R package prereg


Lisa Spitzer, ZPID

*Date: March 16, 2021,
Time: 10:00 / 12:00 h,
Location: Online (Zoom link will be provided)*
 
This workshop is aimed at (psychological) researchers that are relatively new to preregistration or who would like to learn more about different options for creating preregistrations. Specifically, this workshop will be divided into two parts: In the first part, I will illustrate what a preregistration is and why it is important that researchers preregister their studies. In the second part, I will guide the participants through the preregistration process and give practical advice. I will present various possible routes for creating preregistrations, focusing on R Markdown (using the prereg package). 
Furthermore, I will present a new preregistration template that has been developed by members of the American Psychological Association (APA), the British Psychological Society (BPS), the German Psychological Society (DGPs), the Center for Open Science (COS), and the Leibniz Institute for Psychology (ZPID), as well as a new preregistration platform provided by my institute (ZPID).

Since R Markdown will be used, it would be great if the following software could be installed prior to the workshop:

- R (https://cran.r-project.org/)</li>
- RStudio (https://rstudio.com/products/rstudio/#rstudio-desktop download the free "RStudio Desktop - open source edition")</li>
- A TeX distribution (2013 or later; e.g., MikTeX for Windows (https://miktex.org/), MacTeX for Mac (https://tug.org/mactex/), or TeX Live for Linux (https://www.tug.org/texlive/); if you are running Windows, use MikTex if possible, and make sure you install the complete&mdash;not the basic&mdash;version)<br /> </li>

<p><strong>Trainers:</strong></p>
<p>Lisa Spitzer is a second-year PhD student at the Leibniz Institute for Psychology (ZPID), an Open Science institute located in Germany. In her PhD, she focuses on Open Science and especially on preregistration.</p>

Please sign up here: https://forms.gle/kBtzG5p4v1uw17V99



