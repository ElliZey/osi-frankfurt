---
layout:     post
title:      "Announcing winners of Open Science Fellowships"
subtitle:   ""
date:       2020-03-09
author:     "Julia Beitner and Dominik Kraft for the Frankfurt Open Science Initiative"
image:      ""
---

Dear all,

On behalf of the Frankfurt Open Science Initiative we are very proud to announce our first Frankfurt Open Science Fellows for the year 2020 and cordially invite you to their "inaugural talks" and a litte get-together on April 22nd at 4pm (PEG, 5.G170)Isabelle Ehrlich is our PhD Fellow and earned the fellowship for her project „PREMUP-Evidence“ in which she investigates the effect of different levels of Prediction Error on episodic memory performance. Her work stands out because she not only shares all her data and materials, but also draws on public material and integrates it into her work. And our Student Fellow is Leah Kumle with her R-package „mixedpower“. This resource is not only open-source but enables researchers to easily conduct power analyses for (linear) mixed effects model and is thus a potentially far-reaching resource. We are very happy to support these two projects and thus hope to further foster Open Science practices in our department!<p>

See you on April, 22nd! <p>
![picture](/img/fellowship.png)





