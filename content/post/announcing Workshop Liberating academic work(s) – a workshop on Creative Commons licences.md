---
layout:     post
title:      "Workshop: Liberating academic work(s)"
subtitle: "– a workshop on Creative Commons licences"
date:       2022-11-08
author:     "Elli Zey for the Frankfurt Open Science Initiative"
image:      ""
---

We are excited to announce the Workshop "Liberating academic work(s)– a workshop on Creative Commons licences"

The workshop is held by Roland Wagner and Jakob Jung

Date: January, 17 2023
Time: 3pm-4pm 
Location: Online
 
Open Access Publishing is becoming increasingly popular. Thereby the question arises under which license one's work should be published and how one can use licensed material published by others. A Creative Commons (CC) license is one of several public copyright licenses that enable the free distribution of an otherwise copyrighted "work."  Authors can use a CC license to give other people the right to share, use, and build upon a work the author has created. CC provides author flexibility (for example, they might allow only non-commercial uses of a given work). Furthermore, a CC license protects the people who use or redistribute an author's work from copyright infringement concerns as long as they abide by the conditions specified in the license by which the author distributes the work. For further reading, also see: https://creativecommons.org/licenses/

<p> Roland Wagner and Jakob Jung both work in the Open-Access-Team of the Goethe University Library <br>
You can find more information about the Open Access Services of the Goethe University Library here: <a href="https://www.ub.uni-frankfurt.de/publizieren/home_en.html" target="_blank" rel="nofollow">Open Access Publishing at Goethe University</a>
</p>

Please sign up here: https://www.soscisurvey.de/liberating-academic-works/
