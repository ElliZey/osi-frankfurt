---
layout:     post
title:      "Cancellation/postponement of Open Science Fellowships Event on the 22nd of April"
subtitle:   ""
date:       2020-03-31
author:     "Martin Schultze, Dominik Kraft, and Julia Beitner for the Frankfurt Open Science Initiative"
image:      ""
---
Dear all,

we hope this post finds you well. 

Given the circumstances, we are very sad to announce that we must postpone the Open Science Fellowships event on the 22nd of April. 
We will let you know when there is a new date.

Please stay safe and best wishes,
Martin, Dominik, and Julia







