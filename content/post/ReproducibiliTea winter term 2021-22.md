---
layout:     post
title:      "Start of Winter Term 2021/2022 ReproducibiliTea"
subtitle:   "Schedule and Information"
date:       2021-10-27
author:     "Repro-Tea Team for the Frankfurt Open Science Initiative"
image:      ""
---
<strong> Announcing the start of this winter terms ReproducibliTea </strong><br>

Dear All, 

we are very proud to announce: our ReproTea goes in the 4th round: every second Wednesday we meet again from 4-5 pm (sharp, German time, i.e., UTC+1)! This year we have a lot of special guests visiting our Journal Club and we are super excited for the exchange with you and our guests. 

<strong> How to join? </strong><br>
Send an email with subject Join ReproTea to reprotea-join@dlist.server.uni-frankfurt.de or to zey@psych.uni-frankfurt.de to subscribe to our newsletter and receive the Zoom link for the meetings. Find the papers at: https://osf.io/254t7/

Best wishes and we are looking forward to seeing you!
Gözem, Elli, Alexa, and Julia


The schedule:

![picture](/img/ReproTea_schedule_winterterm21-22.png)

 <p>





